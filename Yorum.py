parolalar = [] #parolalarin saklanacagi listeyi olusturduk
while True: # sonsuz döngüye soktuk ki biz istemediğimiz sürece program calismaya devam etsin
    print('Bir işlem seçin') # 3-4-5. satirlarda menumuzu olusturduk
    print('1- Parolaları Listele')
    print('2- Yeni Parola Kaydet')
    islem = input('Ne Yapmak İstiyorsun :') #kullanicidan hangi islemi yapmak istedigini aliyoruz
    if islem.isdigit():   #kullanicinin girdigi islem degiskeninin sayi olup olmadigini kontrol ediyoruz
        islem_int = int(islem)  #kullanicidan girilen her veri string olarak alindigi icin bunu integer bir ifadeye ceviriyoruz ki zaten yukarida sayi kontrolunu yaptıgımız icin bir sikini cikma ihtimali yok
        if islem_int not in [1, 2]:   #kullanicinin girdigi degerin 1 veya 2 olup olmadıgnı kontrol ediyoruz cünkü bizim menumuzde 2 islem var
            print('Hatalı işlem girişi')   #eger 1 veya 2den farkli bir sayi girisi yapılırsa hatali islem girisi ciktisi verip while dongusunun basına geri donuyor
            continue
        if islem_int == 2:     # eger kullanici yeni parola kaydetmek istedigini belirtip 2 girdisi verirse ifin icine giriyor

            girdi_ismi = input('Bir girdi ismi ya da web sitesi adresi girin :')  #kullanicidan girdi parolanın neye ait oldugunu belirtmesini istiyoruz

            kullanici_adi = input('Kullanici Adi Girin :')  #girdigi site veya herhangi bir seyin kullanici adi bilgisi isteniyor

            parola = input('Parola :')    #girdigi kullanici adina ait parola bilgisi isteniyor
            parola2 = input('Parola Yeniden :')    #guvenlik geregi parolanın yanlis olmasini onlemek icin 2. kez parolasini girmesini istiyoruz
            eposta = input('Kayitli E-posta :')    #bu hesaba ait e posta adresini girmesini istiyoruz
            gizlisorucevabi = input('Gizli Soru Cevabı :')   #bu hesaba ait olan gizli sorunun cevabini istiyoruz ki sanki gizli soruyu da sormak guzel olabilirdi :D

            if kullanici_adi.strip() == '':
                print('kullanici_adi girmediz')   #eger ki kullanici adi bos girilmisse hata mesaji yazdirip while dongusunun basına gonderiyor
                continue
            if parola.strip() == '':
                print('parola girmediz')           #eger ki parola bos girilmisse hata mesaji yazdirip while dongusunun basına gonderiyor
                continue
            if parola2.strip() == '':
                print('parola2 girmediz')          #eger ki parola 2 bos girilmisse hata mesaji yazdirip while dongusunun basına gonderiyor
                continue
            if eposta.strip() == '':
                print('eposta girmediz')            #eger ki eposta bos girilmisse hata mesaji yazdirip while dongusunun basına gonderiyor
                continue
            if gizlisorucevabi.strip() == '':
                print('gizlisorucevabi girmediz')      #eger ki gizli sorunun cevabi bos girilmisse hata mesaji yazdirip while dongusunun basına gonderiyor
                continue
            if girdi_ismi.strip() == '':
                print('Girdi ismi girmediz')         #eger ki girdi ismi bos girilmisse hata mesaji yazdirip while dongusunun basına gonderiyor
                continue
            if parola2 != parola:                      # parolalar uyusmazsa hata mesaji yazdirip while dongununun basına gonderiyor
                print('Parolalar eşit değil')
                continue

            yeni_girdi = {     #olusturdugumuz parola bilgilerini dict e atiyoruz
                'girdi_ismi': girdi_ismi,
                'kullanici_adi': kullanici_adi,
                'parola': parola,
                'eposta': eposta,
                'gizlisorucevabi': gizlisorucevabi,
            }
            parolalar.append(yeni_girdi)     #parolalar diye ilk satirda olusturdugumuz listeye yeni girdiyi atiyoruz
            continue     # while dongusunun basina atiyor
        elif islem_int == 1:       #eger ki kullanici parolalari listelemeyi isterse bu elife giriliyor
            alt_islem_parola_no = 0      #alt_islem_parola_no ya 0 atiyoruz nedeni ise hangi parolamiza ait bilgileri yazdirmak istedigimizi secmemiz
            for parola in parolalar:     #parolalar listesini yazdirmak icin kullanilan bir for
                alt_islem_parola_no += 1    #her defasinda birer birer arttirip parolalari siraliyoruz
                print('{parola_no} - {girdi}'.format(parola_no=alt_islem_parola_no, girdi=parola.get('girdi_ismi')))  #ve bu siralanan parolalari satir satir yazdiriyoruz
                                                                                                                        # (NOT: TEK SEFERDE YAPMİYOR BU İSLEMİ. HER SATIT ICIN FOR HER
                                                                                                                        #  SEFERINDE TEKRAR CALISIYOR)
            alt_islem = input('Yukarıdakilerden hangisi ?: ')  #yazdirmis oldugumuz parola listesinden hangisinin bilgilerini yazdirmak istedigini istiyoruz
            if alt_islem.isdigit():    #kullanici girdisinin sayi olup olmadiginin kontrolu saglaniyor. eger sayi ise if e giriliyor
                if int(alt_islem) < 1 and len(parolalar) - 1 < int(alt_islem):     #girilen sayininin yazdirilan listenin disinda olup olmadiginin kontrolu yapiliyor. eger ki disindaysa if e giriliyor
                    print('Hatalı parola seçimi')   # ve hata mesaji ekrana yazdirilip while dongusunun basina donuluyor
                    continue
                parola = parolalar[int(alt_islem) - 1]     #listemizde indexler 0dan basladigi icin -1 yapiyoruz
                print('{kullanici}\n{parola}\n{gizli}\n{eposta}'.format(    #print icinde kullanicinin istedigi parolaya ait bilgileri ekrana yazdiriyoruz
                    kullanici=parola.get('kullanici_adi'),
                    parola=parola.get('parola'),
                    eposta=parola.get('eposta'),
                    gizli=parola.get('gizlisorucevabi'),
                ))
                continue #while dongusunun basina donuluyor

    print('Hatalı giriş yaptınız')    #eger ki kullanici ana menude saf sayi harici bir sey girerse hata mesaji yazdirilip while dongusunun basina doner